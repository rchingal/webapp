from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import AbstractUser
from django.db import models

def create_route_avatar(instance, filename):
    path = 'avatars/{username}/{name}'.format(
        username=instance.username, name=filename
    )
    return path


class PersonAbstract(models.Model):
    """
    Person Abstract model based in profile information
    """

    class GENDER(models.TextChoices):
        NO_SPECIFY = 'no_specify', _('No specify')
        MEN = 'men', _('Men')
        WOMEN = 'women', _('Women')

    class TYPE_DOCUMENT(models.TextChoices):
        NO_SPECIFY = 'no_specify', _('No specify')
        CITIZENDSHIP_CARD = 'citizenship_card', _('Citizenship card')
        IDENTITY_CARD = 'identity_card', _('Identity card')
        FOREIGNER_ID = 'foreigner_id', _('Foreigner ID')
        PASSPORT = 'passport', _('Passport')

    gender = models.CharField(max_length=10, choices=GENDER.choices,
        default=GENDER.NO_SPECIFY, blank=True, null=True, verbose_name=_('gender'))
    address = models.CharField(max_length=30, blank=True, null=True,
        verbose_name=_('address'), help_text=_('residence address.'))
    phone = models.CharField(max_length=30, blank=True, null=True,
        verbose_name=_('Primary phone'), help_text=_('Users phone number.'))
    birth_date = models.DateField(null=True, blank=True, verbose_name=_('birth date'))
    document_number = models.BigIntegerField(null=True, blank=True, unique=True, verbose_name=_('document number'),
        help_text=_('this number is unique and can`t be changed once registered.'))
    type_document = models.CharField(max_length=20, choices=TYPE_DOCUMENT.choices,
        default=TYPE_DOCUMENT.NO_SPECIFY, blank=True, null=True, verbose_name=_('Type document'))
    to_email = models.EmailField(verbose_name=_('to email address'), blank=True, null=True,
        help_text=_('to email address, is the same username'))
    class Meta:
        abstract = True


class User(PersonAbstract, AbstractUser):
    """
    User model based in abstract user for profile information
    """
    avatar = models.ImageField(max_length=255, upload_to=create_route_avatar, blank=True, null=True,
        verbose_name=_('avatar'), help_text=_('profile image of the user'))

    class Meta:
        verbose_name = 'User'
        verbose_name_plural = 'Users'

    def __str__(self):
        return self.username

    @property
    def get_avatar(self):
        from allauth.account.models import EmailAddress
        user = EmailAddress.objects.filter(email__iexact=self.email).first()
        if user:
            return self.avatar
        elif self.avatar:
            return self.avatar.url
        else:
            return None