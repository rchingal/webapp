from django.urls import path, reverse_lazy
from django.contrib.auth import views

from .forms import CustomPasswordResetForm

from .views import (
    LoginView, SignUpView, LogoutView, MyProfile, EditProfileView, PasswordResetConfirmView,
    UserListView, UserCreateView, UserUpdateView, UserDeleteView
)

app_name = "accounts"

urlpatterns = [
    path('accounts/login/', LoginView.as_view(), name=LoginView.name),
    path('accounts/signup/', SignUpView.as_view(), name=SignUpView.name),
    path('accounts/logout/', LogoutView.as_view(), name="logout"),
    path('accounts/password_reset/',
         views.PasswordResetView.as_view(
             template_name='accounts/registration/password_reset_form.html',
             form_class=CustomPasswordResetForm,
             email_template_name='accounts/registration/password_reset_email.html',
             success_url=reverse_lazy('accounts:password_reset_done')
         ), name='password_reset'),
    path('accounts/password_reset/done/', views.PasswordResetDoneView.as_view(
        template_name='accounts/registration/password_reset_done.html'
    ), name='password_reset_done'),
    path('accounts/reset/<uidb64>/<token>/', PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('accounts/reset/done/', views.PasswordResetCompleteView.as_view(
        template_name = 'accounts/registration/password_reset_complete.html'
    ), name='password_reset_complete'),

    path('accounts/password_change/', views.PasswordChangeView.as_view(), name='password_change'),
    path('accounts/password_change/done/', views.PasswordChangeDoneView.as_view(), name='password_change_done'),

    # Profile
    path('accounts/profile/', MyProfile.as_view(), name=MyProfile.name),
    path('accounts/profile/<int:pk>/', EditProfileView.as_view(), name=EditProfileView.name),

    # User
    path('accounts/users/', UserListView.as_view(), name=UserListView.name),
    path('accounts/users/create/', UserCreateView.as_view(), name=UserCreateView.name),
    path('accounts/users/update/<int:pk>/', UserUpdateView.as_view(), name=UserUpdateView.name),
    path('accounts/users/delete/<int:pk>/', UserDeleteView.as_view(), name=UserDeleteView.name),
]