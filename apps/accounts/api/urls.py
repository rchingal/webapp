from django.urls import path, include, reverse_lazy

from .views import ApiRoot
from .views import SignupApiView, MyProfileApiView

#app_name = 'api_accounts'

urlpatterns = [
    # Accounts
    path('accounts/my-profile/', MyProfileApiView.as_view(), name='my-profile'),
    path('accounts/signup/', SignupApiView.as_view(), name=SignupApiView.name),

    # Api Root
	path('', ApiRoot.as_view(), name=ApiRoot.name),
]