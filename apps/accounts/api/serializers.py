from rest_framework import serializers

from ..models import User

class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = [
            'id',
            'email',
            'first_name',
            'last_name',
            'type_document',
            'document_number',
            'birth_date',
            'gender',
            'phone',
            'avatar',
            'address',
        ]