from django.utils.translation import gettext as _


from rest_framework import status
from rest_framework import permissions
from rest_framework import generics

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.reverse import reverse

from .serializers import UserSerializer

from ..forms import SignUpForm


class SignupApiView(APIView):
    """
    Create a new user and register this account
    """
    name = 'Sign Up'

    def post(self, request, version):
        """
        register a new user
        :param request:
        :param version:
        :return: success or serialized errors
        """
        group = self.request.data.get('group', 'host')
        form = SignUpForm(self.request.POST, self.request.FILES)
        if not form.is_valid():
            errors = dict(form.errors.items())
            return Response([{
                'status': 'error',
                'msg': _('can not create user, data errors'),
                'data': errors
            }],
                status=status.HTTP_400_BAD_REQUEST)
        user_instance = form.save()
        serializer = UserSerializer(user_instance)

        return Response([{
            'status': 'success',
            'msg': _('user {} created successfully.').format(user_instance.get_full_name()),
            'version': version,
            'data': serializer.data
        }],
            status=status.HTTP_200_OK)


class MyProfileApiView(APIView):
    """
    endpoint to retrieve the serializer of the user object
    """
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, version):
        serializer = UserSerializer(self.request.user)
        return Response([{
            'status': 'success',
            'msg': _('profile for user {} retrieved.').format(self.request.user.get_full_name()),
            'version': version,
            'data': serializer.data
        }],
            status=status.HTTP_200_OK)


from apps.contacts.api.views import ContactList, CommentList

class ApiRoot(generics.GenericAPIView):

    name = 'api-root'

    def get(self, request, *args, **kwargs):
        return Response({
            'signup': reverse(SignupApiView.name, request=request, kwargs=kwargs),
            'contacts': reverse(ContactList.name, request=request, kwargs=kwargs),
            'comments': reverse(CommentList.name, request=request, kwargs=kwargs),
        })
