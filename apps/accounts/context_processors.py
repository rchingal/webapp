from django.urls import reverse
from django.utils.translation import gettext as _


def Menu(request):
    options = {}
    if request.user.is_authenticated:
        options.update({
            'menu': [
                {'name': _('Dashboard'), 'url': reverse('contacts:contact_list'), 'icon': 'fas fa-tachometer-alt', },
            ]
        })
        options.update({
            'menu_user': [
                {'name': _('my profile'), 'url': reverse('accounts:my_profile'), 'icon': 'fa fa-user', },
                {'name': _('logout'), 'url': reverse('accounts:logout'), 'icon': 'fa fa-power-off', }
            ],
        })
    else:
        options.update({
            'menu_anonymous': [
                {'name': _('home'), 'url': reverse('contacts:home'), 'icon': 'fas fa-home', },
                {'name': _('sign up'), 'url': reverse('accounts:signup'), 'icon': 'fas fa-user-plus', },
                {'name': _('login'), 'url': reverse('accounts:login'), 'icon': 'fas fa-sign-in-alt', },
            ],
        })
    return options