from django.test import TestCase
from django.urls import reverse
from .models import User
from .forms import SignUpForm, CustomAuthenticationForm

class SetupClass(TestCase):

    def setUp(self):
        self.kwars = {}
        self.kwars['username'] = 'chingal'
        self.kwars['email'] = 'email@email.com'
        self.kwars['password'] = 'superpass'
        self.kwars['first_name'] = 'User'
        self.kwars['phone'] = '12345678'
        self.user = User.objects.create_user(**self.kwars)


class LoginFormTest(SetupClass):
    """
    Login form test on `accounts.User`.
    """
    def test_LoginForm_valid(self):
        """
        Valid Login Form Data
        """
        data = {'username': "chingal", 'password': "superpass"}
        form = CustomAuthenticationForm(data=data)
        self.assertTrue(form.is_valid())

    def test_LoginForm_invalid(self):
        """
        Invalid Login Form Data
        """
        data = {'username': "chingal", 'password': "otherpass"}
        form = CustomAuthenticationForm(data=data)
        self.assertFalse(form.is_valid())


class SignUpFormTest(SetupClass):
    """
    SignUp form test on `accounts.User`.
    """
    def test_SignUpForm_valid(self):
        """
        Valid SignUp Form Data
        """
        data = {'username': "admin", 'email': "test1@email.com", 'password1': "superpass123", 'password2': "superpass123", "profile": 0}
        form = SignUpForm(data=data)
        self.assertTrue(form.is_valid())

    def test_SignUpForm_invalid(self):
        """
        # Invalid SignUp Form Data
        """
        data = {'email': "test2@email.com", 'password1': "superuser", 'password2': "superuser1"}
        form = SignUpForm(data=data)
        self.assertFalse(form.is_valid())


class LoginViewsTest(SetupClass):
    """
    Login View test on `accounts.User`.
    """
    def test_login_template(self):
        """
        Validate Login url
        """
        res = self.client.get(reverse('accounts:login'))
        self.assertEqual(res.status_code, 200)

    def test_home_view(self):
        """
        Validate Home view
        """
        user_login = self.client.login(username=self.kwars['username'], password=self.kwars['password'])
        self.assertTrue(user_login)
        res = self.client.get(reverse('contacts:home'))
        self.assertEqual(res.status_code, 200)

    def test_login_view(self):
        """
        Validate Login view
        """
        data = {'username': self.kwars['username'], 'password': self.kwars['password']}
        res = self.client.post('/accounts/login', data)
        self.assertEqual(res.status_code, 301)


class SignUpViewsTest(SetupClass):
    """
    SignUp View test on `accounts.User`.
    """
    def test_signup_template(self):
        """
        Validate SignUp url
        """
        res = self.client.get(reverse('accounts:signup'))
        self.assertEqual(res.status_code, 200)

    def test_signup_view(self):
        """
        Validate SignUp view
        """
        data = {'username': self.kwars['username'], 'password': self.kwars['password'], 'profile': 0}
        res = self.client.post('/accounts/signup', data)
        self.assertEqual(res.status_code, 301)