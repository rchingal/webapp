import os
from celery import task, shared_task
from django.template.loader import get_template, render_to_string
from django.utils.translation import get_language, gettext as _

from utils.Email import send_email_with_template_html

from .models import User

@shared_task
def send_mail_registration(domain, user_id):
    from_email = User.objects.filter(is_superuser=True).first()
    user = User.objects.filter(id=user_id).first()
    if user:
        subject = _('Successful registration')
        email_data = {
            "object": user,
            "domain": domain
        }
        html_content = render_to_string('accounts/mail/notification_register.html', email_data)
        email_res = send_email_with_template_html(subject, from_email.to_email, [user.email], html_content)
    pass
