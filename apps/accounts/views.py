from django.db.models import Q
from django.db import transaction
from django.urls import reverse_lazy
from django.shortcuts import redirect, get_object_or_404
from django.contrib import messages

from django.views.generic import CreateView, UpdateView, ListView, DeleteView
from django.utils.translation import get_language, gettext as _
from django.utils.decorators import method_decorator

from django.contrib.auth.decorators import login_required
from django.contrib.auth import views

from .models import User
from .forms import SignUpForm, CustomAuthenticationForm, EditProfileForm, CustomSetPasswordForm, UserDjangoForm
from .tasks import send_mail_registration


class LoginView(views.LoginView):
    name = 'login'
    form_class = CustomAuthenticationForm
    template_name = 'accounts/registration/login.html'

    def get_success_url(self):
        return reverse_lazy('accounts:my_profile')


class LogoutView(views.LogoutView):
    name = 'logout'


class SignUpView(CreateView):
    """
    Register a New user to the app creating a :model:`accounts.User`.
    """
    name = 'signup'
    model = User
    form_class = SignUpForm
    template_name = 'accounts/registration/signup.html'
    success_url = reverse_lazy('accounts:login')

    @transaction.atomic
    def form_valid(self, form):
        """If the form is valid, save the user as inactive and a confirmation email is sent."""
        sid = transaction.savepoint()
        try:
            user = form.save(commit=False)
            user.username = self.request.POST.get('username')
            user.set_password(self.request.POST.get('password'))
            user.save()
            msg = _('Successful registration. Now you can login..')
            messages.success(self.request, msg)
            transaction.savepoint_commit(sid)
            transaction.on_commit(lambda: send_mail_registration.delay(self.request.get_host(), user.id))
            return super(SignUpView, self).form_valid(form)
        except Exception as e:
            messages.error(self.request, e)
            transaction.savepoint_rollback(sid)
        return super().form_invalid(form)


class PasswordResetConfirmView(views.PasswordResetConfirmView):
    form_class = CustomSetPasswordForm
    template_name = 'accounts/registration/password_reset_confirm.html'

    def form_valid(self, form):
        """If the form is valid, save the user as inactive and a confirmation email is sent."""
        form.save()
        msg = _('Your password has been changed successfully!')
        messages.success(self.request, msg)
        return redirect('accounts:login')


@method_decorator(login_required, name='dispatch')
class MyProfile(UpdateView):
    """
    View to see the profile of the user authenticated.
    """
    name = 'my_profile'
    model = User
    form_class = EditProfileForm

    def get_object(self, **kwargs):
        ## this is to get the object without the user id, override the id with the current user
        return get_object_or_404(User, pk=self.request.user.id)

    def get_success_url(self):
        return reverse_lazy('accounts:my_profile')

    @transaction.atomic
    def form_valid(self, form):
        sid = transaction.savepoint()
        try:
            user = form.save(commit=False)
            user.save()
            msg = _('Your profile has been updated successfully.')
            messages.success(self.request, msg)
            transaction.savepoint_commit(sid)
            return super(MyProfile, self).form_valid(form)
        except Exception as e:
            messages.error(self.request, e)
            transaction.savepoint_rollback(sid)
        return super(MyProfile, self).form_invalid(form)


@method_decorator(login_required, name='dispatch')
class EditProfileView(UpdateView):
    """
    View to edit the profile of the user authenticated.
    """
    name = 'edit_profile'
    model = User
    form_class = EditProfileForm
    template_name = 'accounts/user_detail.html'
    success_url = reverse_lazy('accounts:my_profile')

    def form_valid(self, form):
        msg = _('Your profile has been updated successfully.')
        messages.success(self.request, msg)
        return super(EditProfileView, self).form_valid(form)


@method_decorator(login_required, name='dispatch')
class UserListView(ListView):
    """
    list view for userss
    """
    name = 'user_list'
    model = User
    paginate_by = 5
    ordering = ["-id"]
    template_name = 'accounts/user_list.html.html'

    def get_queryset(self):
        """
        Queryset giving only the current organization
        """
        keyword = self.request.GET.get('keyword', None)
        queryset = self.model.objects.all()

        if keyword:
            queryset = self.model.objects.filter(
                Q(username__icontains=keyword) |
                Q(email__icontains=keyword) |
                Q(first_name__icontains=keyword) |
                Q(last_name__icontains=keyword)
            )
        return queryset


@method_decorator(login_required, name='dispatch')
class UserCreateView(CreateView):
    """
    create view for user
    """
    name = 'user_create'
    form_class = UserDjangoForm
    model = User
    template_name = 'accounts/user_create.html'

    @transaction.atomic
    def form_valid(self, form, **kwargs):
        """
        Validate and save the users on accounts
        """
        sid = transaction.savepoint()
        try:
            self.object = form.save(commit=False)
            self.object.save()
            form.instance = self.object
            form.save()
            msg = _('User has been created successfully.')
            messages.success(self.request, msg)
            transaction.savepoint_commit(sid)
            return redirect(reverse_lazy("accounts:user_list"))
        except Exception as e:
            messages.error(self.request, e)
            transaction.savepoint_rollback(sid)
        return super(UserCreateView, self).form_invalid(form)


@method_decorator(login_required, name='dispatch')
class UserUpdateView(UpdateView):
    """
    update view for user
    """
    name = 'user_update'
    form_class = UserDjangoForm
    model = User
    template_name = 'accounts/user_create.html'

    @transaction.atomic
    def form_valid(self, form, **kwargs):
        """
        Validate and update the user
        """
        sid = transaction.savepoint()
        try:
            self.object = form.save(commit=False)
            self.object.save()
            form.instance = self.object
            form.save()
            msg = _('User has been updated successfully.')
            messages.success(self.request, msg)
            transaction.savepoint_commit(sid)
            return redirect(reverse_lazy("accounts:user_list"))
        except Exception as e:
            messages.error(self.request, e)
            transaction.savepoint_rollback(sid)
        return super(UserUpdateView, self).form_invalid(form)


@method_decorator(login_required, name='dispatch')
class UserDeleteView(DeleteView):
    """
    delete view for user
    """
    name = 'user_delete'
    model = User

    def get_success_url(self):
        """
        url to the detail object
        :return: django url
        """
        return reverse_lazy('accounts:user_list')
