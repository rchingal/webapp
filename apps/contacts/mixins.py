from django.shortcuts import  get_object_or_404
from .models import Contact

class ContactMixin(object):
    """
    Mixin to get contact
    """
    contact = None

    def dispatch(self, *args, **kwargs):
        self.contact = get_object_or_404(Contact, pk=self.kwargs.get("pk")) if self.kwargs.get("pk", None) else None
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.contact:
            context.update({'contact': self.contact})
        return context