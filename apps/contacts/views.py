from django.urls import reverse_lazy
from django.shortcuts import redirect, get_object_or_404
from django.utils.translation import gettext_lazy as _
from django.db import transaction
from django.db.models import Q
from django.db import transaction
from django.contrib import messages
from django.views.generic import ListView, CreateView, UpdateView, DeleteView, DetailView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required

from .models import Contact, Subject, Comment
from .forms import ContactForm, SubjectForm, CommentForm
from .tasks import send_contact_mail_registration
from .mixins import ContactMixin


class HomeView(CreateView):
    """
    Register a New contact by form`.
    """
    name = 'home'
    model = Contact
    form_class = ContactForm
    template_name = 'contacts/home.html'
    success_url = reverse_lazy('contacts:home')

    @transaction.atomic
    def form_valid(self, form):
        sid = transaction.savepoint()
        try:
            contact = form.save(commit=False)
            contact.save()
            msg = _('Thank you for entering your personal data..')
            messages.success(self.request, msg)
            transaction.savepoint_commit(sid)
            transaction.on_commit(lambda: send_contact_mail_registration.delay(self.request.get_host(), contact.id))
            return super(HomeView, self).form_valid(form)
        except Exception as e:
            messages.error(self.request, e)
            transaction.savepoint_rollback(sid)
        return super().form_invalid(form)


@method_decorator(login_required, name='dispatch')
class ContactListView(ListView):
    """
    list view for contacts
    """
    name = 'contact_list'
    model = Contact
    paginate_by = 5
    ordering = ["-id"]

    def get_queryset(self):
        """
        Queryset giving only the current contact
        """
        keyword = self.request.GET.get('keyword', None)
        queryset = self.model.objects.all()

        if keyword:
            queryset = self.model.objects.filter(
                Q(first_name__icontains=keyword) |
                Q(last_name__icontains=keyword) |
                Q(gender__icontains=keyword) |
                Q(subject__name__icontains=keyword)
            )
        return queryset


@method_decorator(login_required, name='dispatch')
class ContactCreateView(CreateView):
    """
    create view for contact
    """
    name = 'contact_create'
    form_class = ContactForm
    model = Contact

    @transaction.atomic
    def form_valid(self, form, **kwargs):
        """
        Validate and save the users on contacts
        """
        sid = transaction.savepoint()
        try:
            self.object = form.save(commit=False)
            self.object.save()
            form.instance = self.object
            form.save()
            msg = _('Contact has been created successfully.')
            messages.success(self.request, msg)
            transaction.savepoint_commit(sid)
            return redirect(reverse_lazy("contacts:contact_list"))
        except Exception as e:
            messages.error(self.request, e)
            transaction.savepoint_rollback(sid)
        return super(ContactCreateView, self).form_invalid(form)


@method_decorator(login_required, name='dispatch')
class ContactUpdateView(UpdateView):
    """
    update view for contact
    """
    name = 'contact_update'
    form_class = ContactForm
    model = Contact

    @transaction.atomic
    def form_valid(self, form, **kwargs):
        """
        Validate and update the contact
        """
        sid = transaction.savepoint()
        try:
            self.object = form.save(commit=False)
            self.object.save()
            form.instance = self.object
            form.save()
            msg = _('User has been updated successfully.')
            messages.success(self.request, msg)
            transaction.savepoint_commit(sid)
            return redirect(reverse_lazy("contacts:contact_list"))
        except Exception as e:
            messages.error(self.request, e)
            transaction.savepoint_rollback(sid)
        return super(ContactUpdateView, self).form_invalid(form)


@method_decorator(login_required, name='dispatch')
class ContactDeleteView(DeleteView):
    """
    delete view for contact
    """
    name = 'contact_delete'
    model = Contact

    def get_success_url(self):
        """
        url to the detail object
        :return: django url
        """
        return reverse_lazy('contacts:contact_list')

@method_decorator(login_required, name='dispatch')
class ContactDetailView(DetailView):
    """
    detail view for contact
    """
    name = 'contact_detail'
    model = Contact
    contact = None

    def dispatch(self, *args, **kwargs):
        self.contact = get_object_or_404(Contact, pk=self.kwargs.get("pk")) if self.kwargs.get("pk", None) else None
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        """
        Update the context data with comment form and comment list.
        """
        data = super(ContactDetailView, self).get_context_data(**kwargs)
        if self.request.POST:
            data['comment_form'] = CommentForm(self.request.POST)
        else:
            data['comment_form'] = CommentForm()
        data['comments'] = Comment.objects.filter(contact_id=self.contact.id).order_by('-created_on')
        return data


@method_decorator(login_required, name='dispatch')
class SubjectListView(ListView):
    """
    list view for subject
    """
    name = 'subject_list'
    model = Subject
    paginate_by = 5
    ordering = ["-id"]

    def get_queryset(self):
        """
        Queryset giving only the current subject
        """
        keyword = self.request.GET.get('keyword', None)
        queryset = self.model.objects.all()

        if keyword:
            queryset = self.model.objects.filter(
                Q(name__icontains=keyword)
            )
        return queryset


@method_decorator(login_required, name='dispatch')
class SubjectCreateView(CreateView):
    """
    create view for subject
    """
    name = 'subject_create'
    form_class = SubjectForm
    model = Subject

    @transaction.atomic
    def form_valid(self, form, **kwargs):
        """
        Validate and save the users on subjects
        """
        sid = transaction.savepoint()
        try:
            self.object = form.save(commit=False)
            self.object.save()
            form.instance = self.object
            form.save()
            msg = _('Contact has been created successfully.')
            messages.success(self.request, msg)
            transaction.savepoint_commit(sid)
            return redirect(reverse_lazy("contacts:subject_list"))
        except Exception as e:
            messages.error(self.request, e)
            transaction.savepoint_rollback(sid)
        return super(SubjectCreateView, self).form_invalid(form)


@method_decorator(login_required, name='dispatch')
class SubjectUpdateView(UpdateView):
    """
    update view for subject
    """
    name = 'subject_update'
    form_class = SubjectForm
    model = Subject

    @transaction.atomic
    def form_valid(self, form, **kwargs):
        """
        Validate and update the subject
        """
        sid = transaction.savepoint()
        try:
            self.object = form.save(commit=False)
            self.object.save()
            form.instance = self.object
            form.save()
            msg = _('User has been updated successfully.')
            messages.success(self.request, msg)
            transaction.savepoint_commit(sid)
            return redirect(reverse_lazy("contacts:subject_list"))
        except Exception as e:
            messages.error(self.request, e)
            transaction.savepoint_rollback(sid)
        return super(SubjectUpdateView, self).form_invalid(form)


@method_decorator(login_required, name='dispatch')
class SubjectDeleteView(DeleteView):
    """
    delete view for subject
    """
    name = 'subject_delete'
    model = Subject

    def get_success_url(self):
        """
        url to the detail object
        :return: django url
        """
        return reverse_lazy('contacts:subject_list')


@method_decorator(login_required, name='dispatch')
class CommentCreateView(ContactMixin, CreateView):
    """
    create view for comment
    """
    name = 'comment_create'
    form_class = CommentForm
    model = Comment

    @transaction.atomic
    def form_valid(self, form, **kwargs):
        """
        Validate and save the comments by contact form
        """
        sid = transaction.savepoint()
        try:
            self.object = form.save(commit=False)
            self.object.author_id = self.request.user.id
            self.object.contact_id = self.contact.id
            self.object.save()
            msg = _('Comment has been created successfully.')
            messages.success(self.request, msg)
            transaction.savepoint_commit(sid)
            return redirect(reverse_lazy("contacts:contact_detail", kwargs={
                    "pk": self.contact.pk
                }))
        except Exception as e:
            messages.error(self.request, e)
            transaction.savepoint_rollback(sid)
        return super(CommentCreateView, self).form_invalid(form)

