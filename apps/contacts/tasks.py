import os
from celery import task, shared_task
from django.template.loader import get_template, render_to_string
from django.utils.translation import get_language, gettext as _

from utils.Email import send_email_with_template_html

from apps.accounts.models import User
from .models import Contact

@shared_task
def send_contact_mail_registration(domain, contact_id):
    from_email = User.objects.filter(is_superuser=True).first()
    obj = Contact.objects.filter(id=contact_id).first()
    if obj:
        subject = _('Successful registration contact form')
        email_data = {
            "object": obj,
            "domain": domain
        }
        html_content = render_to_string('contacts/mail/notification_contact.html', email_data)
        email_res = send_email_with_template_html(subject, from_email.to_email, [obj.email], html_content)
    pass