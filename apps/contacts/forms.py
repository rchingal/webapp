from django import forms
from bootstrap_datepicker_plus import DatePickerInput

from .models import Contact, Subject, Comment

class ContactForm(forms.ModelForm):
    """
    A form that allows entering the contact form data.
    """
    class Meta:

        model = Contact
        fields = '__all__'
        widgets = {
            'first_name': forms.TextInput(attrs={'placeholder': 'First Name', 'class': 'form-control'}),
            'last_name': forms.TextInput(attrs={'placeholder': 'Last Name', 'class': 'form-control'}),
            'birth_date': DatePickerInput(),
            'gender': forms.Select(attrs={'class': 'form-control'}),
            'subject': forms.Select(attrs={'class': 'form-control'}),
            'type_document': forms.Select(attrs={'placeholder': 'Type Document', 'class': 'form-control'}),
            'document_number': forms.NumberInput(attrs={'placeholder': 'Ex. 12345789', 'class': 'form-control'}),
            'phone': forms.TextInput(attrs={'placeholder': 'Phone', 'class': 'form-control'}),
            'email': forms.EmailInput(attrs={'placeholder': 'username@userdirection.com', 'class': 'form-control'}),
            'message': forms.TextInput(attrs={'placeholder': 'Content', 'class': 'form-control'}),
        }


class SubjectForm(forms.ModelForm):
    """
    A form that allows entering the contact form data.
    """
    class Meta:
        model = Subject
        fields = '__all__'
        widgets = {
            'name': forms.TextInput(attrs={'placeholder': 'Name', 'class': 'form-control'}),
        }


class CommentForm(forms.ModelForm):
    """
    A form that allows entering the comment form data.
    """
    class Meta:
        model = Comment
        fields = '__all__'
        exclude = ['author', 'contact', ]
        widgets = {
            'content': forms.Textarea(attrs={'placeholder': 'Content', 'class': 'form-control'}),
        }
