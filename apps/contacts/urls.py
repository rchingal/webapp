from django.urls import path
from django.views.generic import TemplateView

app_name = "contacts"

from .views import (
    HomeView, SubjectListView, SubjectCreateView, SubjectUpdateView, SubjectDeleteView,
    ContactListView, ContactCreateView, ContactUpdateView, ContactDeleteView, ContactDetailView,
    CommentCreateView
)

urlpatterns = [
    path('', HomeView.as_view(), name=HomeView.name),

    # Subject
    path('contacts/subjects/', SubjectListView.as_view(), name=SubjectListView.name),
    path('contacts/subjects/create/', SubjectCreateView.as_view(), name=SubjectCreateView.name),
    path('contacts/subjects/update/<int:pk>/', SubjectUpdateView.as_view(), name=SubjectUpdateView.name),
    path('contacts/subjects/delete/<int:pk>/', SubjectDeleteView.as_view(), name=SubjectDeleteView.name),

    # Contact
    path('contacts/contact/', ContactListView.as_view(), name=ContactListView.name),
    path('contacts/contact/create/', ContactCreateView.as_view(), name=ContactCreateView.name),
    path('contacts/contact/update/<int:pk>/', ContactUpdateView.as_view(), name=ContactUpdateView.name),
    path('contacts/contact/delete/<int:pk>/', ContactDeleteView.as_view(), name=ContactDeleteView.name),
    path('contacts/contact/detail/<int:pk>/', ContactDetailView.as_view(), name=ContactDetailView.name),

    # Comment
    path('contacts/contact/detail/<int:pk>/comments/create/', CommentCreateView.as_view(), name=CommentCreateView.name),

]