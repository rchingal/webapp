from django.db import models
from django.utils.translation import gettext_lazy as _

from apps.accounts.models import PersonAbstract, User

class Subject(models.Model):
    """
    Subject model from contacts app`
    """
    name = models.CharField(max_length=60, verbose_name=_('name'),
        help_text=_('descriptive name of subject'))

    class Meta:
        verbose_name = _('Subject')
        verbose_name_plural = _('Subjects')

    def __str__(self):
        return '{}'.format(self.name)


class Contact(PersonAbstract):
    """
    Contact model from contacts app`
    """
    first_name = models.CharField(max_length=30, verbose_name=_('first name'),
        help_text=_('first name'))
    last_name = models.CharField(max_length=150, verbose_name=_('last name'),
        help_text=_('last name'))
    email = models.EmailField(verbose_name=_('email address'),
        help_text=_('email address, is the same username'))
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    message = models.TextField(verbose_name=_('message'), blank=True, null=True)
    created_on = models.DateTimeField(auto_now_add=True, verbose_name=_('created date'),
        help_text=_('created date'))

    class Meta:
        verbose_name = _('Contact')
        verbose_name_plural = _('Contacts')

    def __str__(self):
        return '{} {}'.format(self.first_name, self.last_name)

    @property
    def get_number_comments(self):
        return self.comment_set.filter(contact_id=self.id).count()


class Comment(models.Model):
    """
    Comment model from contacts app`
    """
    content = models.TextField(max_length=100)
    created_on = models.DateTimeField(auto_now_add=True, verbose_name=_('created date'),
        help_text=_('created date'))
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    contact = models.ForeignKey(Contact, on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('Comment')
        verbose_name_plural = _('Comments')

    def __str__(self):
        return '{} comment {}'.format(self.author.username, self.content)