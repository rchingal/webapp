from rest_framework import serializers
from apps.accounts.api.serializers import UserSerializer
from ..models import Contact, Comment

class ContactSerializer(serializers.ModelSerializer):
    """
    Serializer on Contact model to convert data to JSON or XML.
    """
    class Meta:
        model = Contact
        fields = [
            'id',
            'first_name',
            'last_name',
            'email',
            'gender',
            'subject',
            'document_number',
            'birth_date',
            'phone',
        ]

class CommentSerializer(serializers.ModelSerializer):
    """
    Serializer on comment model to convert data to JSON or XML.
    """
    author = UserSerializer()
    contact = ContactSerializer()

    class Meta:
        model = Comment
        fields = [
            'id',
            'content',
            'author',
            'contact',
            'created_on',
        ]