from django.urls import path
from .views import ContactList, CommentList

urlpatterns = [
    path('contacts/', ContactList.as_view(), name=ContactList.name),
    path('comments/', CommentList.as_view(), name=CommentList.name),
]