from rest_framework import generics
from rest_framework.permissions import IsAuthenticatedOrReadOnly, IsAuthenticated
from rest_framework.authentication import TokenAuthentication

from .serializers import ContactSerializer, CommentSerializer
from .permissions import IsOwnerOrReadOnly
from .pagination import ContactLimitOffsetPagination, ContactPageNumberPagination
from ..models import Contact, Comment


class ContactList(generics.ListCreateAPIView):
    """
    List and create the contacts from database
    """
    name = 'Contact List'
    queryset = Contact.objects.all()
    serializer_class = ContactSerializer
    #authentication_classes = (TokenAuthentication,)
    permission_classes = [IsAuthenticated, IsOwnerOrReadOnly]
    pagination_class = ContactPageNumberPagination


class CommentList(generics.ListCreateAPIView):
    """
    List and create the comments from database
    """
    name = 'Comment List'
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    # authentication_classes = (TokenAuthentication,)
    permission_classes = [IsAuthenticated, IsOwnerOrReadOnly]
    pagination_class = ContactPageNumberPagination
