
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls')),

    path('', include('apps.accounts.urls', namespace='accounts')),
    path('', include('apps.contacts.urls', namespace='contacts')),

    path('accounts/', include('allauth.urls')),

    # Backend API
    path('api/<str:version>/', include('apps.accounts.api.urls')),
    path('api/<str:version>/', include('apps.contacts.api.urls')),

]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns

    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)