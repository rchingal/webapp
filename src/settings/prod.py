from .base import *

DEBUG = True
ALLOWED_HOSTS = ['*']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.environ.get('POSTGRES_DB'),
        'USER': os.environ.get('POSTGRES_USER'),
        'PORT': os.environ.get('DB_PORT'),
        'PASSWORD': os.environ.get('POSTGRES_PASSWORD'),
        'HOST': os.environ.get('DB_HOST'),
    }
}

STATIC_ROOT = os.path.join(os.path.dirname(BASE_DIR), 'static')
print(STATIC_ROOT)
STATICFILES_DIRS = [
   os.path.join(os.path.dirname(BASE_DIR), 'staticfiles')
]

STATIC_URL = '/static/'

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(os.path.dirname(BASE_DIR), 'media')

ACCOUNT_DEFAULT_HTTP_PROTOCOL = 'https'